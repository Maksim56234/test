package ru.unn.recursion;

import java.util.Scanner;

import static jdk.nashorn.internal.objects.ArrayBufferView.length;

public class Recursion {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введите строку");
        String string1 = in.next();
        recurse(string1, string1.length() - 1);
    }

    private static String recurse(String str, int index){
            if(index == -1) {
                return str;
            }
            System.out.print(str.charAt(index));
            recurse(str, index - 1);
            return str;
        }
}